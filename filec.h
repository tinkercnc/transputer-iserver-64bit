

/* ****************************************************************/
#define MAX_FILES       1024      /* Max no of open files */
#define NO_FILE         (FILE *) NULL
#define NO_SLOT         (INT32) -1

struct FD_MAP_INFO {
	FILE           *host_fd;           /* The file descriptor used by the host */
	int            type;          			/* The file type       */
	int						mode;								/* The access mode       */
};

EXTERN struct FD_MAP_INFO FD_MapTab[MAX_FILES];

