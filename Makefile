#
# Iserver Makefile for Linux
# Copyright (C) 1993 Christoph Niemann
#

CC=            cc
#CFLAGS=        -c -O0 -DBOARD_ID=B004 #-m486 
CFLAGS=        -g -O0 -fbuiltin -c -O6 -DBOARD_ID=B004 #-m486 
INSTALLDIR=	/usr/local/bin/


# Objects
OBJ=           iserver.o filec.o hostc.o serverc.o b004link.o

# Targets

iserver.b4:    $(OBJ)
#	$(CC) -s -O6 -o iserver $(OBJ)
	$(CC) -g -s -O0 -o iserver $(OBJ)

#well, this is not very good, but we don't have to change iserver.c
#to avoid the defines you can add the file fcntl.h in the include list
#of iserver.c
iserver.o:   iserver.c iserver.h inmos.h
	$(CC) $(CFLAGS) -DSUN -DO_BINARY=0 -DO_RDONLY=00 iserver.c

filec.o:     filec.c iserver.h pack.h inmos.h filec.h
	$(CC) $(CFLAGS) -DSUN filec.c

hostc.o:     hostc.c iserver.h pack.h inmos.h
	$(CC) $(CFLAGS) hostc.c

serverc.o:   serverc.c iserver.h pack.h inmos.h
	$(CC) $(CFLAGS) serverc.c

#Linux does not support msdosc.c !
#msdosc.o:    msdosc.c iserver.h inmos.h
#	$(CC) $(CFLAGS) msdosc.c

b004link.o:  b004link.c inmos.h   
	$(CC) $(CFLAGS) b004link.c

clean:
	rm -f core *.o iserver

install: iserver.b4
	sudo cp iserver $(INSTALLDIR)
