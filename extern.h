#ifndef _EXTERN_H
#define _EXTERN_H

#include <sys/types.h>
#include <stddef.h>


extern char **environ;
extern char *getenv(const char *name);
extern int ioctl(int fd, unsigned long request, ...);
extern int system(const char *command);
extern int unlink(const char *pathname);
extern ssize_t read(int fd, void *buf, size_t count);
extern ssize_t write(int fd, const void *buf, size_t count);
extern void perror(const char *s);
extern int close(int fd);


#endif  //_EXTERN_H
